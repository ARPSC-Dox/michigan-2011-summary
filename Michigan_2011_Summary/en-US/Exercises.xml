<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Michigan_2010_Summary.ent">
%BOOK_ENTITIES;
]>

<section id="sect-Exercises">
    <title>Exercises</title>
    <para>
      Local programs participate in numerous local and District-wide exercises
      on a regular basis.  There are also a number of statewide exercises in
      which the Section participates.
    </para>
    <section>
      <title>State-sponsored exercises</title>
      <para>
	The State of Michigan performs a number of drills and exercises each
	year, most commonly concerned with nuclear power plants.  During these
	drills and exercises, the State EOC station is activated, the SEC or his
	delegate participates in the EOC, and programs in the affected counties
	are activated.
      </para>
      <para>
	<table frame="all" id="tbl-state-ex-2010">
	  <title>Michigan State Drills and Exercises</title>
	  <tgroup cols="3">
	    <colspec colnum="1" colname="c1" colwidth="1*" />
	    <colspec colnum="2" colname="c2" colwidth="2*" />
	    <colspec colnum="3" colname="c3" colwidth="3*" />
	    <thead>
	      <row>
		<entry>Date</entry>
		<entry>Exercise</entry>
		<entry>Counties</entry>
	      </row>
	    </thead>
	    <tbody>
	      <row>
		<entry>January 21</entry>
		<entry>D. C. Cook Drill #1</entry>
		<entry>Berrien</entry>
	      </row>
	      <row>
		<entry>February 15</entry>
		<entry>D. C. Cook Drill #2</entry>
		<entry>Berrien</entry>
	      </row>
	      <row>
		<entry>March 1</entry>
		<entry>D. C. Cook Full-scale Exercise</entry>
		<entry>Berrien</entry>
	      </row>
	      <row>
		<entry>May 17</entry>
		<entry>New Madrid Aftermath TTX/FE</entry>
		<entry>All</entry>
	      </row>
	    </tbody>
	  </tgroup>
	</table>
      </para>
    </section>
    <section>
      <title>Section Exercises</title>
      <para>
	The Section also organizes statewide exercises each year.  Section
	exercises, unlike State-sponsored exercises, tend to be functional,
	exercising primarily communications skills, and are developed to attempt
	to engage all the counties within the state.
      </para>
      <para>
	The Section has continued to work on the ARES/NTS interface
	and held two drills in 2011 to develop that skill.  A
	Section-wide exercise was held in conjunction with the State's
	New Madrid Aftermath exercise, and the annual SET was also used
	to develop that skill.
      </para>
      <para>
	In addition, also in conjunction with NMA, A drill was held
	with the State of Indiana to exercise sending a specific form
	between state EOCs. Only a relatively few stations
	participated in that drill, which also involved Michigan Army
	MARS and Indiana Navy MARS.
      </para>
      <para>
	One objective for 2011 was to make better use of FEMA's HSEEP
	for exercises.  The Indiana drill moved us quite far in that
	direction, and the October SET followed the FEMA procedures
	quite closely.
      </para>
    </section>
    <section>
      <title>2012 Exercise Plans</title>
      <para>
	Emergency Coordinators have consistently asked for more
	statewide drills, with the most common request being for four
	per year.  However, the FEMA HSEEP process proved to be quite
	burdensome, but also valuable.  It seems unrealistic to expect
	the staff to prepare more than two exercises per year.
      </para>
      <para>
	For the 2012 cycle, our goals are as follows:
	<table frame="none" id="tbl_goals_2012">
	  <title>2011 Objectives</title>
	  <tgroup cols="2">
	    <colspec colnum="1" colname="c1" colwidth="1*" />
	    <colspec colnum="2" colname="c2" colwidth="2*" />
	    <tbody>
	      <row>
		<entry align="right">Timeframe</entry>
		<entry>
		  <itemizedlist>
		    <listitem><para>2012</para></listitem>
		  </itemizedlist>
		</entry>
	      </row>

	      <row>
		<entry align="right">Present Problems</entry>
		<entry>
		  <itemizedlist>
		    <listitem><para>Interface with State Agencies is weak or
			nonexistent</para>
		    </listitem>
		    <listitem><para>Little knowledge of contacting other state
			EOCs</para>
		    </listitem>
		    <listitem><para>Documentation of plans and
		    procedures is badly lacking</para>
		    </listitem>
		  </itemizedlist>
		</entry>
	      </row>
	      <row>
		<entry align="right">Long Range Goal</entry>
		<entry><itemizedlist><listitem><para>Serve effectively as a partner to State Agencies as well as local agencies.</para></listitem></itemizedlist></entry>
	      </row>
	      <row>
		<entry align="right">Functional Objectives</entry>
		<entry><itemizedlist><listitem><para>Develop relationships with some State Agencies</para></listitem><listitem><para>Develop multiple circuits to nearby state EOCs</para></listitem><listitem><para>Continue to develop HSEEP skills</para></listitem></itemizedlist></entry>
	      </row>
	    </tbody>
	  </tgroup>
	</table>
      </para>
      <para>
	The plan for 2012 is as follows:
	<table frame="all" id="tbl-2012-explan">
	  <title>Planned 2011 Exercises</title>
	  <tgroup cols="2" rowsep="1">
	    <colspec colnum="1" colname="c1" colwidth="1*" />
	    <colspec colnum="2" colname="c2" colwidth="3*" />
	    <tbody>
	      <row>
		<entry valign="top" align="right">
		  <filename>Ex12-1</filename> 2Q2012
		</entry>
		<entry>
		  <itemizedlist>
		    <listitem>
		      <para><filename>Exercise:</filename> Functional Drill</para>

		    </listitem>
		    <listitem>
		      <para><filename>For:</filename> Section</para>
		    </listitem>
		    <listitem>
		      <para><filename>Purpose:</filename> Develop digital skills</para>
		    </listitem>
		    <listitem>
		      <para><filename>Rationale:</filename> It has been some time since we exercised HF Digital</para>
		    </listitem>
		  </itemizedlist>
		</entry>
	      </row>
	      <row>
		<entry valign="top" align="right">
		  <filename>Ex12-2</filename> 4Q2012
		</entry>
		<entry>
		  <itemizedlist>
		    <listitem>
		      <para><filename>Exercise:</filename> Functional Drill</para>
		    </listitem>
		    <listitem>
		      <para><filename>For:</filename> Section</para>
		    </listitem>
		    <listitem>
		      <para><filename>Purpose:</filename> Exercise IP from 2011 SET</para>

		    </listitem>
		    <listitem>
		      <para><filename>Rationale:</filename> The 2011
		      SET led to a well considered improvement plan. A
		      year later it would be good to get a checkpoint
		      on progress on that improvement plan.</para>
		    </listitem>
		  </itemizedlist>
		</entry>
	      </row>
	    </tbody>

	  </tgroup>
	</table>


      </para>
    </section>
  </section>
