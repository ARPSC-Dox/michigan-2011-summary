

SELECT SUM(`total`) FROM `sar` WHERE `period` BETWEEN 132 AND 143;
SELECT SUM(`total`) FROM `sar` WHERE `period` BETWEEN 120 AND 131;
SELECT SUM(`total`) FROM `sar` WHERE `period` BETWEEN 108 AND 119;
SELECT SUM(`total`) FROM `sar` WHERE `period` BETWEEN  96 AND 107;
SELECT SUM(`total`) FROM `sar` WHERE `period` BETWEEN  84 AND  95;
SELECT SUM(`total`) FROM `sar` WHERE `period` BETWEEN  72 AND  83;

      sar   qni   qtc
2011 42157 46786 17053
2010 35423 48251 13848
2009 28238 47199  9978
2008 30528 42182  9315
2007 26192 41832 11104
2006 16209 48211  9687


SELECT SUM(`qni`), SUM(`qtc`) from `netreport` WHERE `period`  BETWEEN 132 AND 143;
SELECT SUM(`qni`), SUM(`qtc`) from `netreport` WHERE `period`  BETWEEN 120 AND 131;
SELECT SUM(`qni`), SUM(`qtc`) from `netreport` WHERE `period`  BETWEEN 108 AND 119;
SELECT SUM(`qni`), SUM(`qtc`) from `netreport` WHERE `period`  BETWEEN  96 AND 107;
SELECT SUM(`qni`), SUM(`qtc`) from `netreport` WHERE `period`  BETWEEN  84 AND  95;
SELECT SUM(`qni`), SUM(`qtc`) from `netreport` WHERE `period`  BETWEEN  72 AND  83;


SELECT `call`, SUM(`total`) FROM `sar` WHERE `period` BETWEEN 132 AND 143 GROUP BY `call`  ORDER BY SUM(`total`) DESC

SELECT `call`, SUM(`total`) FROM `bpl` WHERE `period` BETWEEN 132 AND 143 GROUP BY `call`  ORDER BY SUM(`total`) DESC
